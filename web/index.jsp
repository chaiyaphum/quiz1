<%-- 
    Document   : index
    Created on : Dec 13, 2012, 2:32:33 PM
    Author     : Chaiyaphum
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Q1V2 by Chaiyaphum</title>
    </head>
    <body>
        <h1>Class Registration Check</h1>
        <h2>Using XML Parser</h2>

        <form action="ClassChecker">
            URL: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" size="80" maxsize="200" name="url"/>
            <br/><br/>
            Element name: <input type="text" size="40" maxsize="200" name="element"/>
            <br/><br/>
            Keyword: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" size="40" maxsize="200" name="keyword"/>
            <br/>
            <input type="submit" value="Submit"/>
        </form>
        <br/>
        
        <h2>Using XML Transformation</h2>

        <form action="TransformClass">
            XML: <input type="text" size="80" maxsize="200" name="xml_input"/>
            <br/><br/>
            XSL: <input type="text" size="80" maxsize="200" name="xsl_input"/>
            <br/>
            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>
