<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <xsl:variable name="lcletters">abcdefghijklmnopqrstuvwxyz</xsl:variable>
        <xsl:variable name="ucletters">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
        <html>
            <head>
                <title>teacher.xsl</title>
            </head>
            <body>
                <h1>Quiz1 Version2: XSLT by Chaiyaphum Siripanpornchana</h1>
               
                <table border="1">
                    
                    <tr>
                        <th>Name</th>
                        <th>Class</th>
                    </tr>
                    <tr>
                        
                        <xsl:for-each select="//teacher">
                            <xsl:param name="name" select="name"/>
                           
                            <xsl:for-each select="//classes">
                                <xsl:param name="class" select="class"/>
                                <xsl:if test="contains(translate($class,$ucletters,$lcletters), 'xml')">
                                    <tr>
                                        <td>
                                            <xsl:value-of select="$name"/>
                                        </td>
                                        <td>
                                            <xsl:value-of select="$class"/>
                                        </td>
                                    </tr>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:for-each>     
                    </tr>
                </table>

            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
