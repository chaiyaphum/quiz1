package p3;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javassist.convert.Transformer;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author Chaiyaphum
 */
@WebServlet(name = "TransformClass", urlPatterns = {"/TransformClass"})
public class TransformClass extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, TransformerException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        //String xml_input = request.getParameter("xml_input");
        //String xsl_intput = request.getParameter("xsl_intput");
        String xml_input = request.getParameter("xml_input");
        String xsl_intput = request.getParameter("xsl_input");
        //out.print("aaa");
        final File xmlFile = new File("/teacher.xml");
        final File xsltFile = new File("/teacher.xsl");
        System.out.println(XmlTransform.getTransformedHtml(xmlFile, xsltFile));
        out.print(XmlTransform.getTransformedHtml(xmlFile, xsltFile));

    }

    public static class XmlTransform {

        public static String getTransformedHtml(File xmlFile, File xsltFile) throws TransformerException {
            byte[] xml = getStringFromFile(xmlFile).getBytes();
            byte[] xsl = getStringFromFile(xsltFile).getBytes();
            return getTransformedHtml(xml, xsl);
        }

        public static String getTransformedHtml(String xml, String xsl) throws TransformerException {
            return getTransformedHtml(xml.getBytes(), xsl.getBytes());
        }

        public static String getTransformedHtml(byte[] xml, byte[] xsl) throws TransformerException {
            Source srcXml = new StreamSource(new ByteArrayInputStream(xml));
            Source srcXsl = new StreamSource(new ByteArrayInputStream(xsl));
            StringWriter writer = new StringWriter();
            Result result = new StreamResult(writer);
            TransformerFactory tFactory = TransformerFactory.newInstance();
            javax.xml.transform.Transformer transformer = tFactory.newTransformer(srcXsl);
            transformer.transform(srcXml, result);
            return writer.toString();
        }

        private static String getStringFromFile(File f) {
            StringBuilder sb = new StringBuilder(1000);
            try {
                Scanner sc = new Scanner(f);
                while (sc.hasNext()) {
                    sb.append(sc.nextLine());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (TransformerException ex) {
            Logger.getLogger(TransformClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (TransformerException ex) {
            Logger.getLogger(TransformClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
