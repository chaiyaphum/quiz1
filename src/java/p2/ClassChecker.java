/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package p2;

import java.io.*;
import java.net.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

/**
 *
 * @author Chaiyaphum
 */
@WebServlet(name = "ClassChecker", urlPatterns = {"/ClassChecker"})
public class ClassChecker extends HttpServlet {

    XMLEventReader reader;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean teacherFound = true;
        boolean nameFound = false;
        boolean elementFound = false;

        String nameData = null;
        String elementData = null;
        String elementName = null;

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print("<html><body>");
        out.print("<h1>Quiz1 Version2: Parser by Chaiyaphum Siripanpornchana</h1>");

        String inputURL = request.getParameter("url");
        String inputElement = request.getParameter("element");
        String inputKeyword = request.getParameter("keyword");
        out.print("<h2>Searching for element " + inputElement + " that contains xml web page " + inputURL + "</h2>");
        
        try {
            URL u = new URL(inputURL);
            InputStream in = u.openStream();
            // Create object XMLInputFactory for XMLEventReader
            XMLInputFactory factory = XMLInputFactory.newInstance();
            
            //Create object createXMLEventReader for read XML file from URL
            reader = factory.createXMLEventReader(in);
            out.print("<table border = '1'><tr><th>Name</th><th>Course</th ></tr>");

            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();

                // If this event is part of start element
                if (event.getEventType() == XMLEvent.START_ELEMENT) {
                    StartElement startE = event.asStartElement();
                    elementName = startE.getName().getLocalPart();
            
                    if (elementName.equals("teacher")) {
                        teacherFound = true;
                    }
                    if (teacherFound && elementName.equals("name")) {
                        nameFound = true;
                    }
       
                    if (teacherFound && elementName.equals(inputElement)) {
                        elementFound = true;
                    }
                }

                // If this event is part of end element
                if (event.getEventType() == XMLEvent.END_ELEMENT) {
                    EndElement endE = event.asEndElement();
                    elementName = endE.getName().getLocalPart();
        
                    if (elementName.equals("teacher")) {
                        teacherFound = false;
                    }
                    if (elementName.equals("name")) {
                        nameFound = false;
                    }
              
                    if (teacherFound && elementName.equals(inputElement)) {
                        elementFound = false;
                    }
                }

                // If this event is part of characters
                if (event.getEventType() == XMLEvent.CHARACTERS) {
                    Characters chars = event.asCharacters();

                    if (nameFound) {
                        nameData = chars.getData();
                    }
                    if (elementFound) {
                        elementData = chars.getData();

               
                        if (elementData.toLowerCase().contains(inputKeyword.toLowerCase())) {
                            out.print("<tr><td>");
                            out.print(nameData);
                            out.print("</td>");
                            out.print("<td>");
                            out.print(elementData + "</td></tr>");
                        }
                    }
                }
            }
            // end while

            reader.close();
            out.print("</table></body></html>");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            out.close();
        }
    }

    // Method getElemVal will get element value of chlid node
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }
}
